"""

The script takes as its input the OMIM morbid map, a field-delimited file of
OMIM's synopsis of the human gene map (see http://www.omim.org/downloads) and
disease classification into 22 primary disorder classes (see Barabasi paper
on the human disease network, PNAS 2007, SI Table 1). The classification is
based on the physiological system affected by the disorder. The result consists
of three networks in Pajek-based format:

A) diseasome bipartite network,

and two biologically relevant network projections:

B) human disease network (HDN),
C) disease gene network (DGN).

In the HDN nodes represent disorders and two disorders are connected
if they share at least one gene in which mutations are associated with
both disorders.

In the DGN nodes represent disease genes and two genes are connected if
they are associated with the same disorder. The association of gene with the
disorder is supported by various evidence: (1) confirmed association with
unknown underlying effect, (2) linkage, (3) confirmed molecular basis with found
mutation in the genes, (4) a contiguous gene deletion or duplication syndrome,
multiple genes are deleted or duplicated causing the phenotype.

The most complete and best-curated list of known disorder-gene associations is
maintained by the Morbid Map (MM) of the Online Mendelian Inheritance in Man
(OMIM). Each entry of the MM is composed of four fields, the name of the
disorder, the associated gene symbols, its corresponding OMIM ID, and the
chromosomal location.

The script saves information on vertices into .net files, and disease class
assignment into .clu file.

See also:

- OMIM (http://www.omim.org/downloads),
- The human disease network (Goh et al., PNAS 2007).

Script requires Python-levenshtein module (see
https://pypi.python.org/pypi/python-Levenshtein) for fast computation of string
similarities. This is a C extension module. If it is not available user can use
a slower Python implementation contained in script (_levenshtein).

"""

import os
import re
import sys
import Levenshtein
from collections import defaultdict
from itertools import combinations

def main(morbid_map_path, disorder_class_path, output_dir):
    #read OMIM morbid map
    disorder2ind, gene_mim2ind, disorder2gene_mim, node_labels = _read_omim_map(morbid_map_path)
    #read Barabasi disorder classification
    disorder2class, class2ind = _read_disease_classification(disorder_class_path)

    #construct the networks
    _construct_diseasome(disorder2ind, gene_mim2ind, disorder2gene_mim,
                        node_labels, output_dir)
    _construct_hdn(disorder2gene_mim, disorder2ind, output_dir)
    _construct_gdn(disorder2gene_mim, gene_mim2ind, node_labels, output_dir)
    #save disease class assignments
    _write_clu('disease_classification', output_dir, disorder2class,
               disorder2ind, class2ind)

def _read_omim_map(morbid_map_path):
    disorder2gene_mim = defaultdict(set)
    gene_mim2gene_names = defaultdict(list)
    for line in open(morbid_map_path):
        line = line.strip().split('|')
        disorder, gene_names, gene_mim, cytogen = line

        tmp = disorder.split(',')
        disorder = _parse_disorder(tmp)

        gene_names = [g.strip() for g in gene_names.split(',')]
        gene_mim = gene_mim.split(',')

        disorder2gene_mim[disorder].update(gene_mim)
        for gm in gene_mim:
            gene_mim2gene_names[gm].extend(gene_names)

    disorder2ind = {disorder:i+1 for i, disorder in enumerate(disorder2gene_mim)}
    gene_mim2ind = {gene_mim:i+1 for i, gene_mim in enumerate(gene_mim2gene_names)}

    node_labels = {g:names[0] for g, names in gene_mim2gene_names.iteritems()}
    node_labels.update({d:d for d in disorder2ind})

    return disorder2ind, gene_mim2ind, disorder2gene_mim, node_labels

def _read_disease_classification(disorder_class_path):
    disorder2class = {}
    f = open(disorder_class_path)
    f.readline()
    for line in f:
        line = line.strip().split('\t')

        tmp = line[1].replace('"', '').split(',')
        disorder = _parse_disorder(tmp)

        disorder_class = line[-1].strip()
        disorder2class[disorder] = disorder_class
    f.close()

    classes = set(disorder2class.values())
    class2ind = {cls:i+1 for i, cls in enumerate(classes)}

    return disorder2class, class2ind

def _construct_diseasome(disorder2ind, gene_mim2ind, disorder2gene_mim,
                        node_labels, output_dir):
    #save diseasome bipartite network
    node2ind = {n:i+1 for i, n in enumerate(disorder2ind.keys()
                                          + gene_mim2ind.keys())}

    edges = []
    for d1, genes in disorder2gene_mim.iteritems():
        for gene in genes:
            edges.append((d1, gene))

    _write_net('diseasome', output_dir, node2ind, edges,
     node_labels=node_labels, two_mode=len(disorder2ind))

def _construct_hdn(disorder2gene_mim, disorder2ind, output_dir):
    #save human disease network (HDN)
    edges = []
    for d1, d2 in combinations(disorder2ind.keys(), 2):
        g1 = disorder2gene_mim[d1]
        g2 = disorder2gene_mim[d2]
        if len(g1.intersection(g2)) > 0:
            edges.append((d1, d2, len(g1.intersection(g2))))

    _write_net('hdn', output_dir, disorder2ind, edges)

def _construct_gdn(disorder2gene_mim, gene_mim2ind, node_labels, output_dir):
    #save disease gene network (DGN)
    gene_mim2disorder = defaultdict(set)
    for d, genes in disorder2gene_mim.iteritems():
        for gene in genes:
            gene_mim2disorder[gene].add(d)

    edges = []
    for g1, g2 in combinations(gene_mim2ind.keys(), 2):
        d1 = gene_mim2disorder[g1]
        d2 = gene_mim2disorder[g2]
        if len(d1.intersection(d2)) > 0:
            edges.append((g1, g2, len(d1.intersection(d2))))

    _write_net('dgn', output_dir, gene_mim2ind, edges,
               node_labels=node_labels)

def _levenshtein(w1, w2):
    """
    Calculates the Levenshtein distance between w1 and w2.
    :param w1:
    :param w2:
    :return:
    """
    n, m = len(w1), len(w2)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        w1,w2 = w2,w1
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if w1[j-1] != w2[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]

def _parse_disorder(tmp):
    if len(tmp[0]) < 5:
        disorder = tmp[0] + ',' + tmp[1]
    else:
        disorder = tmp[0]

    disorder = disorder.replace('[', '').replace(']', '')
    disorder = disorder.replace('{','').replace('}', '')
    disorder = re.sub(r'\(\d\)', '', disorder).strip()

    return disorder

def _write_net(name, output_dir, node2ind, edges, node_labels=None, two_mode=False):
    path = os.path.join(output_dir, "%s.net" % name)
    print "Writing: %s" % path
    print "# vertices: %d" % len(node2ind)
    print "# edges: %s" % len(edges)
    f = open(path, 'w')
    if not two_mode:
        f.write("*Vertices %d\n" % len(node2ind))
    else:
        f.write("*Vertices %d %d\n" % (len(node2ind), two_mode))

    ind2node = {i:n for n, i in node2ind.iteritems()}
    for i in xrange(1, len(ind2node)+1):
        if not node_labels:
            f.write(' %d "%s"\n' % (i, ind2node[i]))
        else:
            f.write(' %d "%s"\n' % (i, node_labels[ind2node[i]]))

    f.write("*Edges\n")
    if len(edges[1])==3:
        for e1, e2, val in edges:
            f.write(' %d %d %d\n' % (node2ind[e1], node2ind[e2], val))
    else:
        for e1, e2 in edges:
            f.write(' %d %d\n' % (node2ind[e1], node2ind[e2]))
    f.close()

def _write_clu(name, output_dir, node2prop, node2ind, prop2ind):
    path = os.path.join(output_dir, "%s.clu" % name)
    print "Writing: %s" % path
    print 'Partitions of vertices\n', '\n'.join('%30s %d' % (k,v) for k, v in
                                              prop2ind.iteritems())
    f  = open(path, 'w')
    f.write("*Vertices %d\n" % len(node2ind))

    node_props = {}
    for node, ind in node2ind.iteritems():
        if node in node2prop:
            prop_ind = prop2ind[node2prop[node]]
        else:
            #string-match, levenshtein distance (jaro similarity)
            #if node does not have assigned property then give it the
            #property of the nearest node
            dist = [(Levenshtein.jaro(node, n), n) for n in node2prop]
            dist = sorted(dist, reverse=True)
            prop_ind = prop2ind[node2prop[dist[0][1]]]
        node_props[ind] = prop_ind

    for i in xrange(1, len(node_props)+1):
        f.write(" %d\n" % node_props[i])
    f.close()

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print "Usage: python %s <path-to-OMIM-morbid-map-file> " \
              "<path-to-Barabasi-classification> " \
              "<path-to-output-dir>" % sys.argv[0]
    else:
        main(sys.argv[1], sys.argv[2], sys.argv[3])